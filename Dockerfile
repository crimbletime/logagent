FROM node:10-alpine

RUN \
  apk add --update bash git && \
  rm -rf /var/cache/apk/* && \
  mkdir -p /logs && \
  git clone https://gitlab.com/crimbletime/logagent.git && \
  cd logagent && \
  npm install -g --unsafe-perm

COPY bro-ids.yaml /

ENTRYPOINT ["/bin/bash"]

